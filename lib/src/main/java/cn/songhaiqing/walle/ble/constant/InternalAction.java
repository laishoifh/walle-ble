package cn.songhaiqing.walle.ble.constant;

/**
 * 内部Action，不对外
 */
public class InternalAction {
    public static final String RESULT_FINISH = "cn.songhaiqing.walle.ble.internal.ACTION_RESULT_FINISH"; // 单条结果返回成功
    public static final String READ_BLE = "cn.songhaiqing.walle.ble.internal.ACTION_READ_BLE";
    public static final String WRITE_BLE = "cn.songhaiqing.walle.ble.internal.ACTION_WRITE_BLE";
    public static final String CONNECT_DEVICE = "cn.songhaiqing.walle.ble.internal.ACTION_CONNECT_DEVICE";
    public final static String START_SCAN = "cn.songhaiqing.walle.ble.internal.ACTION_START_SCAN"; // 开始扫描设备
    public final static String STOP_SCAN = "cn.songhaiqing.walle.ble.internal.ACTION_STOP_SCAN"; // 结束扫描设备
    public static final String DISCONNECT_DEVICE = "cn.songhaiqing.walle.ble.internal.ACTION_DISCONNECT_DEVICE";
    public final static String EXECUTED_SUCCESSFULLY = "cn.songhaiqing.walle.ble.internal.ACTION_EXECUTED_SUCCESSFULLY";
    public final static String EXECUTED_FAILED = "cn.songhaiqing.walle.ble.internal.ACTION_EXECUTED_FAILED";
}
