package cn.songhaiqing.walle.ble.utils;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import cn.songhaiqing.walle.ble.constant.Constants;
import cn.songhaiqing.walle.ble.constant.InternalAction;
import cn.songhaiqing.walle.ble.service.WalleBleService;

public class BleUtil {
    private static final String TAG = "BleUtil";
    private static int connectStatus = 0;
    public static final int CONNECT_STATUS_NOT_CONNECTED = 0;   // 未连接
    public static final int CONNECT_STATUS_CONNECTING = 1;      // 连接中
    public static final int CONNECT_STATUS_SUCCESS = 2;         // 已连接
    public static final int CONNECT_STATUS_FAIL = 3;            // 连接失败

    public static String bleAddress;
    public static String bleName;

    /**
     * 连接设备
     *
     * @param address MAC 地址
     */
    public synchronized static boolean connectDevice(final Context context, final String address) {
        connectStatus = CONNECT_STATUS_CONNECTING;
        boolean success = true;
        if (!ToolUtil.isServiceRunning(WalleBleService.class.getName(), context)) {
            LogUtil.i(TAG, "WalleBleService服务未启动，开始启动");
            Intent intent = new Intent(context, WalleBleService.class);
            intent.putExtra("macAddress", address);
            try {
                context.startService(intent);
            } catch (Exception e) {
                LogUtil.e(TAG, "WalleBleService启动失败");
                success = false;
            }
        } else {
            LogUtil.i(TAG, "WalleBleService已启动，开始发送连接广播");
            Intent intent = new Intent(InternalAction.CONNECT_DEVICE);
            intent.putExtra(Constants.EXTRA_DATA, address);
            context.sendBroadcast(intent);
        }
        return success;
    }

    /**
     * 断开连接
     *
     * @param context
     */
    public synchronized static void disConnect(final Context context) {
        connectStatus = CONNECT_STATUS_NOT_CONNECTED;
        if(!ToolUtil.isServiceRunning(WalleBleService.class.getName(), context)){
            LogUtil.d(TAG, "WalleBleService未运行，无需要执行断开操作。");
            return;
        }
        context.sendBroadcast(new Intent(InternalAction.DISCONNECT_DEVICE));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(context, WalleBleService.class);
                context.stopService(intent);
            }
        }, 500);
    }

    @Deprecated
    public static void broadcastReadBle(Context context, byte[] bytes, String serviceUUID,
                                        String characteristicUUID) {
        if (getConnectStatus() != CONNECT_STATUS_SUCCESS) {
            return;
        }
        Intent intent = new Intent(InternalAction.READ_BLE);
        intent.putExtra(Constants.EXTRA_DATA_READ_SERVICE_UUID, serviceUUID);
        intent.putExtra(Constants.EXTRA_DATA_READ_CHARACTERISTIC_UUID, characteristicUUID);
        intent.putExtra(Constants.EXTRA_DATA, bytes);
        context.sendBroadcast(intent);
    }

    /**
     * 读取蓝牙设备数据
     *
     * @param context
     * @param serviceUUID        服务UUID
     * @param characteristicUUID 特征值UUID
     */
    public static void readBle(Context context, String serviceUUID, String characteristicUUID) {
        if (getConnectStatus() != CONNECT_STATUS_SUCCESS) {
            return;
        }
        Intent intent = new Intent(InternalAction.READ_BLE);
        intent.putExtra(Constants.EXTRA_DATA_READ_SERVICE_UUID, serviceUUID);
        intent.putExtra(Constants.EXTRA_DATA_READ_CHARACTERISTIC_UUID, characteristicUUID);
        context.sendBroadcast(intent);
    }

    /**
     * 蓝牙设备写入命令
     *
     * @param context
     * @param notifyServiceUUID        订阅服务UUID
     * @param notifyCharacteristicUUID 订阅特征值UUID
     * @param writeServiceUUID         写入服务UUID
     * @param writeCharacteristicUUID  写入特征值UUID
     * @param bytes                    写入内容
     */
    public static void broadcastWriteBle(Context context, String notifyServiceUUID,
                                         String notifyCharacteristicUUID, String writeServiceUUID,
                                         String writeCharacteristicUUID, byte[] bytes) {
        if (getConnectStatus() != CONNECT_STATUS_SUCCESS) {
            return;
        }
        writeBle(context, notifyServiceUUID, notifyCharacteristicUUID, writeServiceUUID,
                writeCharacteristicUUID, bytes, true, false);
    }

    /**
     * 蓝牙设备写入命令
     *
     * @param context
     * @param notifyServiceUUID        订阅服务UUID
     * @param notifyCharacteristicUUID 订阅特征UUID
     * @param writeServiceUUID         写入服务UUID
     * @param writeCharacteristicUUID  写入特征UUID
     * @param bytes                    命令内容
     * @param segmentation             是否分包发送，true  以最多20个字节会包发送
     */
    public static void broadcastWriteBle(Context context, String notifyServiceUUID,
                                         String notifyCharacteristicUUID, String writeServiceUUID,
                                         String writeCharacteristicUUID, byte[] bytes, boolean segmentation) {

        writeBle(context, notifyServiceUUID, notifyCharacteristicUUID, writeServiceUUID,
                writeCharacteristicUUID, bytes, segmentation, false);
    }

    /**
     * 蓝牙设备写入命令
     *
     * @param context
     * @param notifyServiceUUID        订阅服务UUID
     * @param notifyCharacteristicUUID 订阅特征UUID
     * @param writeServiceUUID         写入服务UUID
     * @param writeCharacteristicUUID  写入特征UUID
     * @param bytes                    命令内容
     * @param segmentation             是否分包发送，true  以最多20个字节会包发送
     * @param immediately              是否立即发送（因命令有队列机制，需要优选执行，如：测量终止测量手环心率等场景）
     */
    public static void broadcastWriteBle(Context context, String notifyServiceUUID,
                                         String notifyCharacteristicUUID, String writeServiceUUID,
                                         String writeCharacteristicUUID, byte[] bytes, boolean segmentation,
                                         boolean immediately) {

        writeBle(context, notifyServiceUUID, notifyCharacteristicUUID, writeServiceUUID,
                writeCharacteristicUUID, bytes, segmentation, immediately);
    }

    /**
     * 发送写入命令
     *
     * @param context
     * @param notifyServiceUUID        订阅服务UUID
     * @param notifyCharacteristicUUID 订阅特征UUID
     * @param writeServiceUUID         写入服务UUID
     * @param writeCharacteristicUUID  写入特征UUID
     * @param bytes                    命令内容
     * @param segmentation             是否分包发送，true  以最多20个字节会包发送
     * @param immediately              是否立即执行，默认false
     */
    private static void writeBle(Context context, String notifyServiceUUID,
                                 String notifyCharacteristicUUID, String writeServiceUUID,
                                 String writeCharacteristicUUID, byte[] bytes, boolean segmentation,
                                 boolean immediately) {
        if (getConnectStatus() != CONNECT_STATUS_SUCCESS) {
            return;
        }
        Intent intent = new Intent(InternalAction.WRITE_BLE);
        intent.putExtra(Constants.EXTRA_DATA_NOTIFY_SERVICE_UUID, notifyServiceUUID);
        intent.putExtra(Constants.EXTRA_DATA_NOTIFY_CHARACTERISTIC_UUID, notifyCharacteristicUUID);
        intent.putExtra(Constants.EXTRA_DATA_WRITE_SERVICE_UUID, writeServiceUUID);
        intent.putExtra(Constants.EXTRA_DATA_WRITE_CHARACTERISTIC_UUID, writeCharacteristicUUID);
        intent.putExtra(Constants.EXTRA_DATA, bytes);
        intent.putExtra(Constants.EXTRA_DATA_WRITE_SEGMENTATION, segmentation);
        intent.putExtra(Constants.EXTRA_DATA_IMMEDIATELY, immediately);
        context.sendBroadcast(intent);
    }

    /**
     * 获取连接状态
     *
     * @return {@link BleUtil#CONNECT_STATUS_NOT_CONNECTED}
     * {@link BleUtil#CONNECT_STATUS_CONNECTING}
     * {@link BleUtil#CONNECT_STATUS_SUCCESS}
     * {@link BleUtil#CONNECT_STATUS_FAIL}
     */
    public static int getConnectStatus() {
        return connectStatus;
    }

    public static void setConnectStatus(int connectStatus) {
        BleUtil.connectStatus = connectStatus;
    }

    /**
     * 判断连接是否可用
     *
     * @return
     */
    public static boolean bleIsEnabled() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            return false;
        }
        return bluetoothAdapter.isEnabled();
    }

    /**
     * 验证或开启蓝牙
     *
     * @param activity
     * @param resultCode
     * @return
     */
    public static boolean validOrOpenBle(Activity activity, int resultCode) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            return false;
        }
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, resultCode);
            return false;
        } else {
            return true;
        }
    }

    /**
     * 开始扫描设备
     *
     * @param context
     */
    public static void startScan(final Context context) {
        startScan(context, null);
    }

    /**
     * 开始扫描设备
     *
     * @param context
     * @param scanFilterName 过滤名称
     */
    public static void startScan(final Context context, final String[] scanFilterName) {

        if (!ToolUtil.isServiceRunning(WalleBleService.class.getName(), context)) {
            Intent intent = new Intent(context, WalleBleService.class);
            context.startService(intent);
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        sleep(100);
                    } catch (InterruptedException e) {
                        LogUtil.e("BleUtil", e.getMessage());
                    }
                    Intent intent = new Intent(InternalAction.START_SCAN);
                    if (scanFilterName != null) {
                        intent.putExtra("scanFilterName", scanFilterName);
                    }
                    context.sendBroadcast(intent);
                }
            }.start();
        } else {
            Intent intent = new Intent(InternalAction.START_SCAN);
            if (scanFilterName != null) {
                intent.putExtra("scanFilterName", scanFilterName);
            }
            context.sendBroadcast(intent);
        }
    }

    /**
     * 停止扫描设备
     *
     * @param context
     */
    public static void stopScan(Context context) {
        Intent intent = new Intent(InternalAction.STOP_SCAN);
        context.sendBroadcast(intent);
    }

    /**
     * 关闭蓝牙连接服务
     *
     * @param context
     */
    public static void stopWalleBleService(Context context) {
        Intent intent = new Intent(context, WalleBleService.class);
        context.stopService(intent);
    }

    /**
     * 命令有队列机制，成功返回结果后调用该方法可立即执行下一条命令。
     * 如不调用该方法则只能等待超时后执行自动执行下一个命令
     *
     * @param context
     */
    public static void finishResult(Context context) {
        context.sendBroadcast(new Intent(InternalAction.RESULT_FINISH));
    }
}
